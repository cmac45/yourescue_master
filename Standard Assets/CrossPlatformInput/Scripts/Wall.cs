﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {

	public Sprite damagedSprite;
	public int hp = 1;
	private int dmg = 1;
	private SpriteRenderer sR;
	private GameObject player;

	void Awake () 
	{
		sR = GetComponent<SpriteRenderer>();
		player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// wall damage
	void DamageWall () 
	{
		//sR.sprite = damagedSprite;

		hp -= dmg;
		if(hp <= 0)
			gameObject.SetActive(false);
	}
}
