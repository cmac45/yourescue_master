﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SaveCommand : MonoBehaviour {



	public static void Save()
	{
		SaveData ();
	}

	static void SaveData() 
	{
		
		using (ES2Writer writer = ES2Writer.Create("aoesavedata.txt")) 
		{
			int score = Player.Score;
			int health = Player.Health;
			//List<string> items = Player.items;

			writer.Write(score, "Score");
			writer.Write(health, "Health");

			writer.Save ();
		}
		
	}

	void SaveData<T>(T param, string tag)
	{
		ES2.Save (param, "aoesavedata.txt?tag=" + tag);

	}
}
