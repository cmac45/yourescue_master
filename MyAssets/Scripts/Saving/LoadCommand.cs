﻿using UnityEngine;
using System.Collections;

public class LoadCommand : MonoBehaviour {
	
	

	public static void Load()
	{
		LoadData ();
	}

	//TODO put data back in place
	static void LoadData()
	{
		
		if(ES2.Exists ("aoesavedata.txt"))
		{
			using(ES2Reader reader = ES2Reader.Create("aoesavedata.txt"))
			{
				var score = reader.Read<int>("Score");
				var health = reader.Read<int>("Health");

				Player.Score = score;
				Player.Health = health;

				HUDManager.UpdateHealth(health);
				HUDManager.UpdateScore(score);
			}
		}
		
	}


//	void LoadData<T>(T param, string tag)
//	{
//
//	}
}
