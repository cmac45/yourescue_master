﻿using UnityEngine;
using System.Collections;
//using MoodkieSecurity.EasySav

public class SaveManager : MonoBehaviour {


	public static SaveManager  m_This = null;
	

	void Awake()
	{
		if(m_This == null)
		{
			//If I am the first instance, make me the Singleton
			m_This = this;
			DontDestroyOnLoad(this); 
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != m_This)
				Destroy(this.gameObject);
		}
	}

	void Start () 
	{
		this.name = "SaveManager";
	}

	
	
	//for testing purposes only
	//call save methods on event
	void Update () {
		
		if(Input.GetKeyDown(KeyCode.V))
		{
			SaveCommand.Save ();
		}
		if(Input.GetKeyDown(KeyCode.L))
		{
			LoadCommand.Load ();
		}
	}


}
