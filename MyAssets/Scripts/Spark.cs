﻿using UnityEngine;
using System.Collections;

public class Spark : MonoBehaviour {

	private GameObject wire;
	public float forceMod = 1.0F;
	private float baseForceX = 1.0F;
	private float baseForceY = 1.0F;
	Rigidbody2D wireRb;

	// Use this for initialization
	void Start () {
		wire = GameObject.FindGameObjectWithTag("wire");
		wireRb = wire.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame

	void Wiggle (){
		baseForceX = Random.Range(1,10) * forceMod;
		baseForceY = Random.Range(1,10) * forceMod;
		Vector2 forceVec = new Vector2(baseForceX, baseForceY);
		do {
			wireRb.AddForce(forceVec);
			StartCoroutine(wait());
		}while(wire.activeSelf);
	}

	IEnumerator wait(){
		yield return new WaitForSeconds(1.0f);
	}
}
