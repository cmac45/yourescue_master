﻿using UnityEngine;
using System.Collections;

public class OilSlick : MonoBehaviour {


	//private GameObject Oil;
	private GameObject player1;
	// Use this for initialization
	void Awake () {
		//Oil = GameObject.FindGameObjectWithTag("Oil");
		player1 = GameObject.FindGameObjectWithTag("Player");

	}


	void OnTriggerEnter2D (Collider2D col) {
		//oil = Oil.gameObject.GetComponent<BoxCollider2D>();
		if(col.tag == "Player"){
			player1.gameObject.GetComponent<PlayerMainScript>().inOil = true;
			player1.gameObject.GetComponent<Rigidbody2D>().angularDrag = 0.0f;
			player1.gameObject.GetComponent<Rigidbody2D>().drag = 0.0f;
		}
	}

	void OnTriggerExit2D (Collider2D col){
		//oil = Oil.gameObject.GetComponent<BoxCollider2D>();
		if(col.tag == "Player")
			player1.gameObject.GetComponent<PlayerMainScript>().inOil = false;
	}
}
