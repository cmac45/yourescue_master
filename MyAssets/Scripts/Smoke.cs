﻿using UnityEngine;
using System.Collections;

public class Smoke : MonoBehaviour {

	// Use this for initialization
	public GameObject player1;
	void Awake () {
		player1 = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void OnTriggerStay2D (Collider2D col) {
		if(col.tag == "Player" && player1.GetComponent<PlayerMainScript>().gasMask == true)
		{
			gameObject.GetComponent<ParticleSystem>().GetComponent<Renderer>().enabled = false;
		}
		else if (player1.GetComponent<PlayerMainScript>().gasMask == false)
		{
			gameObject.GetComponent<ParticleSystem>().GetComponent<Renderer>().enabled = true;
		}
	}
}
