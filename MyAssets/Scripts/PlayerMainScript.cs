﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerMainScript : MonoBehaviour 
{

	public float damp = 0.02f;

	public GameObject mainPlayer;
	public bool inOil = false;
	public bool collided = false;
	public bool gasMask = false;
	private Vector2 still = new Vector2(0.0f,0.0f);
	private static float scale = 3.33f;
	private Vector3 downScale = new Vector3(scale, scale, scale);
	
	//Vector2 moveVector;
	Rigidbody2D rb; 

	void Start ()
	{
		rb = this.GetComponent<Rigidbody2D>();
		mainPlayer = GameObject.FindGameObjectWithTag("Player");
	}
	
	void Update ()
	{

		Vector2 moveVector = new Vector2(CrossPlatformInputManager.GetAxis("Horizontal"), CrossPlatformInputManager.GetAxis("Vertical")) * damp;

		if(inOil == false){
			rb.AddForce(moveVector);
		}
		else if(inOil == true && collided == true){
			rb.AddForce(moveVector);
		}

		float rotateH = moveVector.x * -1.0f;
		float rotateV = moveVector.y;
		
		if(CrossPlatformInputManager.GetAxis("Horizontal") != 0.0f && CrossPlatformInputManager.GetAxis("Vertical") != 0.0f){
			float angle = Mathf.Atan2(rotateH,rotateV) * Mathf.Rad2Deg;
			rb.MoveRotation(angle);

		}
		if(mainPlayer.transform.localScale == downScale){
			damp = 1.33f;
		}
		if(CrossPlatformInputManager.GetButtonDown("GasMask") == true){
			//Debug.Log("testOn");
			gasMask = true;
		}
		if(CrossPlatformInputManager.GetButtonUp("GasMask") == true){
			//Debug.Log("testOff");
			gasMask = false;
		}
		if(CrossPlatformInputManager.GetButtonDown("Extinguisher") == true){
			extinguish();
		}

	}

	void extinguish(){
		
	}

	/*void GasMaskOn ()
	{
		Debug.Log("testOn");
		gasMask = true;
	}
	void GasMaskOff()
	{
		Debug.Log("testOff");
		gasMask = false;
	}*/

}
