﻿using UnityEngine;
using System.Collections;

public class CameraFollowScript : MonoBehaviour 
{

	#region CAMERA VARIABLES 
	public Transform                      m_target; // target for Camera to follow, i.e. Hero
	public float                          m_damping = 1;
	public float                          m_lookAheadFactor = 3;
	public float                          m_lookAheadReturnSpeed = 0.5f;
	public float                          m_lookAheadMoveThreshold = 0.1f;
	
	private float                         m_offsetZ;
	private Vector3                       m_lastTargetPosition;
	private Vector3                       m_currentVelocity;
	private Vector3                       m_lookAheadPos;
	#endregion
	
	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start()
	{
		m_lastTargetPosition = m_target.position;
		m_offsetZ = transform.position.z - m_target.position.z;
		transform.parent = null;
	}
	
	/// <summary>
	/// Have the camera follow the player
	/// </summary>
	void Update()
	{
		// only update lookahead pos if accelerating or changed direction
		float xMoveDelta = m_target.position.x - m_lastTargetPosition.x;
		bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > m_lookAheadMoveThreshold;

		if (updateLookAheadTarget)
		{
		//	m_lookAheadPos = m_lookAheadFactor *Vector3.right *Mathf.Sign(xMoveDelta);
		}
		else
		{
			m_lookAheadPos = Vector3.MoveTowards(m_lookAheadPos, Vector3.zero, Time.deltaTime * m_lookAheadReturnSpeed);
		}

		Vector3 aheadTargetPos = m_target.position + m_lookAheadPos + Vector3.forward * m_offsetZ;
		Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref m_currentVelocity, m_damping);
		transform.position = newPos;
		m_lastTargetPosition = m_target.position;
	}
}