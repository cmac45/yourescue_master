﻿using UnityEngine;
using System.Collections;

public class CameraMovementScript : MonoBehaviour {

	public float moveSpeed = 2.0f;
	
	void Update () 
	{
		transform.Translate (0, moveSpeed * Time.deltaTime, 0);
	}
}
