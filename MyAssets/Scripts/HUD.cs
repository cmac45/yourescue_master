﻿using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour {

	public static HUD  m_This = null;
	
	void Awake()
	{
		if(m_This == null)
		{
			//If I am the first instance, make me the Singleton
			m_This = this;
			DontDestroyOnLoad(this); 
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != m_This)
				Destroy(this.gameObject);
		}
	}
}
