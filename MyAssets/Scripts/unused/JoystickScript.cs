﻿using UnityEngine;
using System.Collections;

public class JoystickScript : MonoBehaviour {

	static public JoystickScript        m_This = null;
	static public float x, y;

	public float                        m_maxSpeed = 0.1f;
	public float                        m_moveSpeed = 0.1f;


	void Awake()
	{
		m_This = this;
	}

	void Update () 
	{
		if (Input.touches.Length > 0)
		{
			if (Input.touches[0].phase == TouchPhase.Moved)
			{
				x = Input.touches[0].deltaPosition.x * m_maxSpeed * Time.deltaTime;
				y = Input.touches[0].deltaPosition.y * m_maxSpeed * Time.deltaTime;
			}
			else if (Input.touches[0].phase == TouchPhase.Ended)
			{ 
				x = 0;
				y = 0;
			}
		}
	
	} // end of Update

} // end of class