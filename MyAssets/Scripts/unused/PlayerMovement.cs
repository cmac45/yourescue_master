﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
	
	public float                       m_maxSpeed = 0.1f;
	public float                       m_moveSpeed = 0.1f;

	public float smooth = 2.0f;
	public float tiltAngle = 30.0f;
	//float tiltAroundZ;
	float tiltAroundX;
	Quaternion target;
	
	
	void Update()
	{

		if (Input.GetKey(KeyCode.A))
		{
			transform.Translate (-m_moveSpeed * Time.smoothDeltaTime, 0, 0);

		}
		
		
		if (Input.GetKey(KeyCode.D))
		{
			transform.Translate (m_moveSpeed * Time.smoothDeltaTime, 0, 0);

		}
		
		if (Input.GetKey(KeyCode.S))
		{
			transform.Translate (0, -m_moveSpeed * Time.smoothDeltaTime, 0);

		}
		
		if (Input.GetKey(KeyCode.W))
		{
			transform.Translate (0, m_moveSpeed * Time.smoothDeltaTime, 0);

		}


		//---------------------touch-----------------


		if (Input.touches.Length > 0)
		{
			if (Input.touches[0].phase == TouchPhase.Moved)
			{
				var x = Input.touches[0].deltaPosition.x * m_maxSpeed * Time.deltaTime;
				var y = Input.touches[0].deltaPosition.y * m_maxSpeed * Time.deltaTime;
				//Debug.Log("x: " + x + " ; " + "y: " + y);
				
				transform.Translate( new Vector3(x, y, 0)); 
				
			}
		}



	}
	
//	void Joystick () 
//	{
//
//		if (Input.touches.Length > 0)
//		{
//			if (Input.touches[0].phase == TouchPhase.Moved)
//			{
//				var x = Input.touches[0].deltaPosition.x * m_maxSpeed * Time.deltaTime;
//				var y = Input.touches[0].deltaPosition.y * m_maxSpeed * Time.deltaTime;
//				//Debug.Log("x: " + x + " ; " + "y: " + y);
//				
//				transform.Translate( new Vector3(x, y, 0)); 
//				
//			}
//		}
//
//	}


	
	
	
	
	
	
	
	//	private Rigidbody2D                m_rigidbody2DCache = null;
	//	private Vector2                    m_heading;
	//	private Vector2                    m_movement;
	//
	//	void Awake()
	//	{
	//		m_This = this;
	//		m_rigidbody2DCache = this.GetComponent<Rigidbody2D> ();
	//		m_heading = Vector2.zero;
	//	}
	//
	//
	//
	//	void Update()
	//	{
	//		if (Input.touchCount > 0)
	//		{
	//
	//			Vector2 touchPosition = Input.GetTouch(0).position;
	//			Vector3 touchWorldPos = Camera.main.ScreenToWorldPoint(new Vector3(touchPosition.x, touchPosition.y, 0)); //?
	//			Vector3 charPosition = this.gameObject.transform.position;
	//			this.m_heading = new Vector2(touchWorldPos.x - charPosition.x, touchWorldPos.y - charPosition.y);
	//			this.m_heading.Normalize();
	//		}
	//	}
	//
	//
	//	void FixedUpdate()
	//	{
	//		float xMovement = Input.GetAxis ("Horizontal");
	//		float yMovement = Input.GetAxis ("Vertical");
	//		m_movement = new Vector2 (xMovement, yMovement);
	//		Debug.Log ("xmovement is " + xMovement + ", zmovement is " + yMovement);
	//
	//		if (m_movement.magnitude > 0)
	//		{
	//			Debug.Log ("mag > 0");
	//			this.m_heading = m_movement.normalized; 
	//		}
	//
	//		Move (m_heading);
	//
	//	}
	//
	//	void Move(Vector2 heading)
	//	{
	//
	//		gameObject.rigidbody2D.velocity = new Vector2 (m_movement.x * m_maxSpeed, m_movement.y * m_maxSpeed);
	//	}
	
	
	
	
	
}