﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {


	public static MenuManager  m_This = null;
	
	public Canvas main;

	public GameObject optionsPanel;
	public GameObject buttonsPanel;

	public List<int> levels = new List<int> ();
	public List<Button> buttonsList;
	
	public static int lastID = 1;
	//public int LastID { get { return lastID; } set {lastID = value;} }

	void Awake()
	{
		if(m_This == null)
		{
			//If I am the first instance, make me the Singleton
			m_This = this;
			DontDestroyOnLoad(this); 
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != m_This)
				Destroy(this.gameObject);
		}
	}
	
	void Start () 
	{
		main.gameObject.SetActive (true);
		optionsPanel.gameObject.SetActive (false);
		ActivateButtons ();

	}


	public int sector = 1; //controls the range of level numbers
	/// <summary>
	/// Activates the appropriate buttons.
	/// </summary>
	void ActivateButtons () 
	{
		for(int i = 1; i < buttonsList.Count; i++)
		{
			var buttonValue = i + 6 * (sector-1);
			buttonsList[i].gameObject.GetComponentInChildren<Text>().text = buttonValue.ToString();

			if (i > lastID)
			{
				buttonsList[i].interactable = false;
			}
			else 
			{
				buttonsList[i].interactable = true;
			}
		}
	}



	public void NewGame()
	{
		//TODO start new game
		// GameManager.newgame
	}
	
	public void ContinueGame()
	{
		//TODO load stuff 
	}
	
	public void Store()
	{
		//TODO 
	}

	public void Achievements()
	{

		//TODO view Achievements (and Leaderboards)
	}
	
	public void Options(bool isInsideMainMenu)
	{
		buttonsPanel.gameObject.SetActive (!isInsideMainMenu);
		optionsPanel.gameObject.SetActive (isInsideMainMenu);
	}


}
