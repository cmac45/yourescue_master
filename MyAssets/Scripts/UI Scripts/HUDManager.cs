﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour {

	public static HUDManager  m_This = null;



	static Canvas hudCanvas;
	static Text healthText;
	static Text scoreText;
		
	void Awake()
	{
		if (m_This == null) {
			//If I am the first instance, make me the Singleton
			m_This = this;
			DontDestroyOnLoad (this); 
		} else {
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if (this != m_This)
				Destroy (this.gameObject);
		}

		hudCanvas = GameObject.Find ("HUD").GetComponent<Canvas> ();
		healthText = GameObject.Find ("HealthText").GetComponent<Text> ();
		scoreText = GameObject.Find ("ScoreText").GetComponent<Text> ();

	}

	void Start()
	{
		hudCanvas.gameObject.SetActive (false);
		UpdateHealth (Player.Health);
		UpdateScore (Player.Score);
	}

	#region HUD object

	public static void ShowHUD(bool b)
	{

		hudCanvas.gameObject.SetActive (b);
	}
	#endregion


	#region Update Info
	public static void UpdateScore(int i)
	{
		scoreText.text = i.ToString ();
	}


	public static void UpdateHealth(int i)
	{
		healthText.text = i.ToString ();
	}
	
	#endregion


	//TODO health bar above head


}
