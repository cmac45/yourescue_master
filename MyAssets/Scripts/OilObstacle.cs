﻿using UnityEngine;
using System.Collections;

public class OilObstacle : MonoBehaviour {

	private GameObject player1;
	// Use this for initialization
	void Awake () {
		player1 = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D col) {
		if(col.tag == "Player")
		player1.gameObject.GetComponent<PlayerMainScript>().collided = true;
	}

	void OnTriggerExit2D(Collider2D col) {
		if(col.tag == "Player")
		player1.gameObject.GetComponent<PlayerMainScript>().collided = false;
	}	
}
