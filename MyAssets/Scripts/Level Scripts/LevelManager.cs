﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour {


	public static LevelManager  m_This = null;
		
	void Awake()
	{
		if(m_This == null)
		{
			//If I am the first instance, make me the Singleton
			m_This = this;
			DontDestroyOnLoad(this); 
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != m_This)
				Destroy(this.gameObject);
		}
	}


	void OnLevelWasLoaded()
	{
		if (Application.loadedLevel > 0)
			HUDManager.ShowHUD (true);
	}

	public void Load_Level(int i)
	{
		LoadLevel (i);
		
	}


	public static void LoadLevel(int i)
	{
		Application.LoadLevel (i);
	}

	public static void LoadNextLevel()
	{

		var i = Application.loadedLevel;
		Application.LoadLevel (i + 1);
	}

	public static void OnLevelBegin()
	{
	}


	public static void OnLevelEnd()
	{
	}

	
}
