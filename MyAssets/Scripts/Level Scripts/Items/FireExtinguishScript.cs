﻿using UnityEngine;
using System.Collections;

public class FireExtinguishScript : MonoBehaviour {
	

	public GameObject dustBullet;

	void OnTriggerEnter2D (Collider2D col) 
	{
		if (col.name == dustBullet.name) 
		{
			Destroy(gameObject);
			Destroy(col.gameObject);
		}
	}
}
