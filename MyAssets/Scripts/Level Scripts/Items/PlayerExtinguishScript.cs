﻿using UnityEngine;
using System.Collections;

public class PlayerExtinguishScript : MonoBehaviour {

	public Rigidbody2D                      m_bulletPrefab;
	public float                            m_fireSpeed = 15f;
	private float                           m_attackSpeed = 0.5f;
	private float                           m_coolDown;
	private bool                            m_canShoot = false;
	

	void Update () 
	{
	
		if ( Time.time >= m_coolDown) 
		{
			m_canShoot = true;
		}
		else {
			m_canShoot = false;
		}
	}

	
	public void ExtinguishFunc() 
	{
		Debug.Log ("I'm shooting");

		if (m_canShoot) {
		    Rigidbody2D clone = Instantiate (m_bulletPrefab, transform.position, transform.rotation) as Rigidbody2D;
		    //clone.transform.parent = transform.parent;
		    clone.GetComponent<Rigidbody2D>().AddForce (transform.up * m_fireSpeed*2);
		    m_coolDown = Time.time + m_attackSpeed;
		}

	}
}