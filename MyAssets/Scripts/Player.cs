﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour {

    //singleton
	public static Player  m_This = null;

	private static int score = 0;
	private static int health = 100;
	public static int Score { get {return score;} set {score = value;} }
	public static int Health  { get {return health;} set {health = value;} }
	internal static List<string> items = new List<string>();

	void Awake()
	{
		if(m_This == null)
		{
			//If I am the first instance, make me the Singleton
			m_This = this;
			DontDestroyOnLoad(this); 
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != m_This)
				Destroy(this.gameObject);
		}
	}


	void Start()
	{

	}



	# region Health
	public static void UpdateHealth(int i)
	{

		health -= i;
		if (health <= 0)
		{

		}

		HUDManager.UpdateHealth (health);

	}
	#endregion


	#region Score
	public static void UpdateScore(int i)
	{

		score += i;
		HUDManager.UpdateScore (score);
	}
	#endregion




	# region Inventory
	public static void addItem(string item)
	{
		items.Add (item);
	}

	public static void removeItem(string item)
	{
		items.Remove (item);
	}

	public static bool isInInventory(string item)
	{
		return items.Contains (item);
	}

	public static void printInventory()
	{
		foreach (var i in items)
			Debug.Log (i);
	}

	#endregion


	
	//health
	//inventory
	//item interaction?

}
