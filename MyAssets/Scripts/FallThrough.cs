using UnityEngine;
using System.Collections;

public class FallThrough : MonoBehaviour {
	
	private GameObject player1;
	private GameObject level1;
	private GameObject tunnel;
	private Camera camera1;
	public GameObject fallzone;
	public float fallSpeed = 0.2F;
	private float newScale = 3.33f;
	public bool fall = false;
	
	// Use this for initialization
	void Start () {
		player1 = GameObject.FindGameObjectWithTag("Player");
		level1 = GameObject.FindGameObjectWithTag("level1");
		camera1 = Camera.main;
		fallzone = GameObject.FindGameObjectWithTag("hole");
		tunnel = GameObject.FindGameObjectWithTag("tunnel");
		tunnel.SetActive(false);
	}
	
	// Update is called once per frame


	 void OnTriggerStay2D (Collider2D hole){
		if(camera1.orthographicSize >= 1.0f){
		camera1.orthographicSize -= fallSpeed;
		}
		if(camera1.orthographicSize <= 1.0f){
			fallzone.SetActive(false);
		}
		player1.transform.localScale = new Vector3(newScale, newScale, newScale);
		level1.SetActive(false);
		tunnel.SetActive(true);
		fallzone.gameObject.GetComponent<SpriteRenderer>().enabled = false;
		player1.transform.position = new Vector3(-0.02f, 17.80f, 0.0f);
		
	}



	
}